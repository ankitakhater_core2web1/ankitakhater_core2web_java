class SwitchDemo{
        public static void main(String[] args){
                String friends = "Aboli";
                System.out.println("Before Switch");
                switch(friends){
                        case "Pratu":
                                System.out.println("Pratiksha");
                                break;

                        case "Aboli":
                                System.out.println("Aboli");
                                break;

                        case "Annu":
                                System.out.println("Anvi");
                                break;
                        case "Vaishu":
                                System.out.println("Vaishnavi");

                        default:
                                System.out.println("In default State");


                }

                                System.out.println("After Switch");
        }

}

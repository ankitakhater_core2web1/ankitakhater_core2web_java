class InputDemo{
        void methodFun(){
                System.out.println("In fun function");
        }
        void methodRun(){
                System.out.println("In run method");
        }
        void methodGun(){
                System.out.println("In gun method");
	}
        public static void main(String[] args){
                System.out.println("In main method");
                methodFun();
		methodRun();
		methodGun();
        }
}

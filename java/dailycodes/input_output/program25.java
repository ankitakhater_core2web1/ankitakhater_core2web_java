import java.io.*;
class InputDemo{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Company Name :");
                String cmpName = br.readLine();
                System.out.print("Enter employee Name :");
                String empName = br.readLine();
		System.out.print("Enter Employee Id :");
	        int empId = Integer.parseInt(br.readLine());
                System.out.println("Company Name : " + cmpName);
                System.out.println("Employee Name : " + empName);
		System.out.println("Employee Id :" + empId);
        }
}

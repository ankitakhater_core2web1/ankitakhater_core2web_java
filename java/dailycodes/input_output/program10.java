import java.util.Scanner;

class ScannerDemo{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter name :");
                String name = sc.next();
                System.out.println("Enter College Name: ");
                String clgname = sc.next();
                System.out.println("Enter Student id:");
                int stuid = sc.nextInt();
                System.out.println("Enter CGPA :");
                float marks = sc.nextFloat();
		System.out.println("Student name: " + name);
		System.out.println("College Name: " + clgname);
		System.out.println("Student ID: " + stuid);
		System.out.println("Marks: " + marks);
	}
}

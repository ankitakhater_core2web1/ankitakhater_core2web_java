import java.util.*;
class Char{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the number of row:");
                int rows = sc.nextInt();
		char ch = 'A';

                for(int i = rows;i>=1;i--){
                        for(int j = rows;j>=i;j--){
                                System.out.print(ch + " ");
				ch++;
                        }
			ch--;
                        System.out.println();
                }
        }
}

import java.util.*;

class Array{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("Size:");
                int size = sc.nextInt();
                int arr[] = new int[size];
		int sum = 0;

                System.out.println("Enter the elements:");

                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("Sum of odd elements in array is : ");
                for(int j = 0; j<arr.length; j++){
			
			if(arr[j]%2==1){
				sum = arr[j]+sum;
			}
		}
                System.out.println(sum);
        }
}

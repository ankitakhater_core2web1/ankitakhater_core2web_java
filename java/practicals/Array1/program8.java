import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner (System.in);
                System.out.println("Enter count of employees in company:");
                int size = sc.nextInt();
                int arr[] = new int[size];
		int count = 1;

                System.out.println("Enter ages");

                for(int i = 0; i<arr.length; i++){
                	arr[i]= sc.nextInt();
                }

                System.out.println("Ages of employees working in a company");

                for(int j = 0; j<arr.length; j++){
                                System.out.println("Age of Employee " + count++ + " is: " +arr[j]);

                }
        }
}

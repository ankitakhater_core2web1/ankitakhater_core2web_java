import java.util.*;

class Array{
	public static void main(String[] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("size:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements:");

		for(int i = 0; i<arr.length; i++){
			arr[i]= sc.nextInt();
		}	

		System.out.println("Elements in an array which are less than 10 are:");

		for(int j = 0; j<arr.length; j++){
			if(arr[j] < 10){

				System.out.println(arr[j]);
			}	

		}
	}
}

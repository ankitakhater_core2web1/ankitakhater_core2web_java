import java.util.*;

class Array{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Size:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements:");

		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Array Elements are : ");
		for(int j = 0; j<arr.length; j++){
			System.out.println(arr[j]);
		}
	}
}

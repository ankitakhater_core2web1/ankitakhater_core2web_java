import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
                for(int i =0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
		System.out.println("Array Elements are:");
                if(size%2==0){
                	for(int j = 0; j<size ; j+=2){
			       System.out.println(arr[j]);
                        }
		}
		else{
			for(int k = 0; k<size; k++){
				System.out.println(arr[k]);
			}
                }
        }
}

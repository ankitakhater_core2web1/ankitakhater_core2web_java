import java.util.*;

class ArrayMin{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];
               
                System.out.println("Enter the Elements:");
                for(int i =0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
		int num = arr[0];
                for(int j=0; j<arr.length ; j++){
                        if(num>arr[j]){
                               num = arr[j];
                        }
                }
                System.out.println("Minimum number in the array is: " + num);
        }
}

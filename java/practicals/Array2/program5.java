import java.util.*;

class ArrayOdd{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];
		int sum = 0;

                System.out.println("Enter the Elements:");
                for(int i =0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
                for(int j=0; j<arr.length ; j++){
                        if(j%2==1){
                               sum = arr[j]+sum;
                        }
                }
		System.out.println("Sum of odd indexed elements: " + sum);
        }
}

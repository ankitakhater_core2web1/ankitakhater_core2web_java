import java.util.*;

class ArraySearch{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
                for(int i =0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
		System.out.println("Enter the element search in array:");
		int element = sc.nextInt();
		
		for(int j=0; j<arr.length ; j++){
                	if(element == arr[j]){                
				System.out.println(arr[j] + " found at index " + j );
                        }
                }

        }
}


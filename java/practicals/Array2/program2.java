import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                int sum = 0;

                System.out.println("Enter the elements:");

                for(int i = 0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
                System.out.println("Elements divisible by 3 are :");
                for(int j = 0; j<arr.length; j++){
                        if(arr[j]%3==0){
                                System.out.print(arr[j] + " ");
                                sum = arr[j]+sum;
                        }
                }
                System.out.println();
                System.out.println("Sum of Elements divisible by 3 are: " + sum);
        }
}

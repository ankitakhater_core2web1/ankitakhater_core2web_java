import java.util.*;

class ArrayOdd{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                int mul = 1;

                System.out.println("Enter the Elements:");
                for(int i =0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
                for(int j=0; j<arr.length ; j++){
                        if(j%2==1){
                               mul = arr[j]*mul;
                        }
                }
                System.out.println("Product of odd indexed elements: " + mul);
        }
}

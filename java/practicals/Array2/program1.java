import java.util.*;


class ArrayEven{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size = sc.nextInt();
                int arr[] = new int[size];
		int count = 0;

		System.out.println("Enter the elements:");

		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Even numbers are :"); 
		for(int j = 0; j<arr.length; j++){
			if(arr[j]%2==0){
		 		System.out.print(arr[j] + " ");
				count++;
			}
		}
		System.out.println();
		System.out.println("Count of Even Numbers are: " + count);
	}
}

			       

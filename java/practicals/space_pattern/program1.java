import java.util.Scanner;
class Demo{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no. of rows");
                int row = sc.nextInt();

                for(int i = row;i>=1;i--){
                        for(int j=1;j<i;j++){
                                System.out.print(" ");
			}
			for(int k = 0;k<=row-1 ;k++){
                        System.out.print("*");
			}
                System.out.println();
		}
        }
}

import java.util.*;

class ArrayChar{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("size:");
                int size = sc.nextInt();
                char arr[] = new char[size];

                System.out.println("Enter elements:");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);
                }
		System.out.println("Elements of an array: ");

                for(int j=0; j<arr.length; j++){
                        char ch = Character.toUpperCase(arr[j]);
                        System.out.print(ch + " ");
                }
                        System.out.println();
        }
}

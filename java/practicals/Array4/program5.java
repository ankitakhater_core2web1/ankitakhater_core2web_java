import java.util.*;

class ArrayReverse{
        public static void main(String[] args){

        	Scanner sc = new Scanner(System.in);
        	System.out.println("size:");
        	int size = sc.nextInt();
        	int arr[] = new int[size];

        	System.out.println("Enter elements:");

        	for(int i=0; i<arr.length; i++){
                	arr[i] = sc.nextInt();
        	}
		System.out.println("Reversed Array is:");
        	for(int j=(size-1); j>=0; j--){
			System.out.println(arr[j]);
                
        	}
        }
}

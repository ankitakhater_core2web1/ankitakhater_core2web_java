import java.util.*;

class Array{
        public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("size:");
        int size = sc.nextInt();
        int arr[] = new int[size];
	int max = arr[0];

        System.out.println("Enter elements:");

        for(int i=0; i<arr.length; i++){
                arr[i] = sc.nextInt();
        }
        Arrays.sort(arr);
        System.out.println("Second largest element in an array " + arr[size-2]);
        }
}

import java.util.*;

class ArrayChar{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("size:");
                int size = sc.nextInt();
                char arr[] = new char[size];

                System.out.println("Enter elements:");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);
                }
                 
		System.out.println("Elements in an array is: ");
                for(int j=0; j<arr.length; j++){
			char ch = Character.toLowerCase(arr[j]);
                        if (ch >= 'a' && ch <= 'z'){
                                System.out.println(arr[j]);
                        }
			else{
				System.out.println("#");
			}
                }
        }
}

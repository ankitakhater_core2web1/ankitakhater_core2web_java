import java.util.*;

class ArrayChar{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter size:");
                int size = sc.nextInt();
                char arr[] = new char[size];

                System.out.println("Enter elements:");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);
                }

                System.out.println("Enter character key: ");
                char ch = sc.next().charAt(0);
                
		System.out.println("Array: ");
                for(int j=0; j<arr.length; j++){
                        if (ch == arr[j]){
				break;
                        }
			else{
				System.out.println(arr[j]);
			}
                }

        }
}

import java.util.*;

class ArrayChar{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("size:");
                int size = sc.nextInt();
                char arr[] = new char[size];
		int vowels = 0;
		int consonant = 0;

                System.out.println("Enter elements:");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.next().charAt(0);
                }
       
                for(int j=0; j<arr.length; j++){
		        char ch = Character.toLowerCase(arr[j]);
			if(ch == 'a' || ch =='e' || ch == 'i' || ch == 'o' || ch == 'u'){
				vowels++;
			}
			else{
				consonant++;
			}
		}
                        System.out.println("Count of Vowels is " + vowels);
			System.out.println("Count of Consonants is " + consonant);
        }
}

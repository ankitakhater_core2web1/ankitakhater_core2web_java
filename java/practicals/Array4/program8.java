import java.util.*;

class ArrayChar{
        public static void main(String[] args){

        	Scanner sc = new Scanner(System.in);
       		System.out.println("size:");
        	int size = sc.nextInt();
        	char arr[] = new char[size];
		int count = 0;

        	System.out.println("Enter elements:");

        	for(int i=0; i<arr.length; i++){
                	arr[i] = sc.next().charAt(0);
        	}

        	System.out.println("Enter the specific character : ");
        	char ch = sc.next().charAt(0);

        	for(int j=0; j<arr.length; j++){
               		if (ch == arr[j]){
                       		count++;
               		}
		}	
                System.out.println(ch + " is occurs " + count + " times in given array");
  
       	}
}

import java.util.*;

class ArrayChar{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();
                char arr[] = new char[size];

                System.out.println("Enter the elements for array:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.next().charAt(0);
                }
                System.out.println("Array alternate elements before reverse:");

                for(int i = 0; i<size; i+=2){
                        System.out.println(arr[i]);
                }
		System.out.println("Array alternate elements after reverse:");
		for(int i = size; i>=1; i--){
			i--;
                        System.out.println(arr[i]);
                }
        }
}

import java.util.*;

class ArrayMultiple{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();
                int arr[] = new int[size];
		boolean flag = false;

                System.out.println("Enter the elements for array:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.nextInt();
                }
		System.out.println("Enter Key: ");
		int key = sc.nextInt();

                 for(int i = 0; i<size; i++){
			 if(key==arr[i]){
				flag = true;
			 }
		 }
		 if(flag==false){
			 System.out.println("Element not fount in an array");
		 }
		 for(int i = 0;i<size;i++){
				 if(arr[i]%key==0){
					 System.out.println("An element multiple of " + key + " found at index " + i);
				 }	 
		 }

        }
}

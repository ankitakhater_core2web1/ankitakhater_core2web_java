import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();

                int arr1[] = new int[size];
		int arr2[] = new int[size];

                System.out.println("Enter the elements for array1:");
                for(int i = 0; i<size; i++){
                        arr1[i]=sc.nextInt();
                }
		System.out.println("Enter the elements for array2:");
		for(int i = 0; i<size; i++){
                        arr2[i]=sc.nextInt();
                }
                System.out.println("Common elements in an array are : ");
                for(int j = 0; j<size;j++){
			for(int i = 0;i<size;i++){
                        	if(arr1[j]==arr2[i]){
					System.out.print(arr1[j] + ", ");
				}
			}
		}
		System.out.println();
	}
}

                                

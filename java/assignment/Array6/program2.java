import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();
		int sum =0;
		int primeCount =0;
		boolean isPrime = false;

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.nextInt();
                }
                for(int j = 0; j<size; j++){
			int num = arr[j];
			int count =0;
			if(num<=1){
				isPrime = false;
			}
			else{
				for(int k  = 1;k<=num;k++){
					if(num%k == 0){
					count++;
					}				}
			}
			if(count == 2){
				isPrime = true;
			}
			else{
				isPrime = false;
			}
			if(isPrime){
				primeCount++;
				sum += num;
			}
		}
		System.out.println("The sum of prime numbers in an array : " + sum + " and count of prime number in an array is " + primeCount);
	}
}

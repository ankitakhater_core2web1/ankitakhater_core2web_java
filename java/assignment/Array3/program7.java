import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Size:");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the elements:");

                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("Array: ");
		if((size%2==1) && (size>=5)){
			System.out.println("Odd Elements:");
                	for(int j = 0; j<arr.length ; j++){
                              if(arr[j]%2==1){

				      System.out.println(arr[j]);
			      }
			}
		}
	        else{ 
			System.out.println("Even Elements:");
			for(int i = 0;i<arr.length;i++){
				if(arr[i]%2==0){
					System.out.println(arr[i]);
                		}
			}	
        	}
	}
}

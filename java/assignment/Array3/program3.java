import java.util.*;

class Occurance{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter size:");
                int size = sc.nextInt();
                int arr[] = new int[size];

                System.out.println("Enter elements: ");
                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter specific number: ");
                int num = sc.nextInt();
                boolean found = false;
		int count = 0;

                for(int j = 0; j<arr.length; j++){
                        if(num == arr[j]){
                                found = true;
				count++;
                        }
                }
                if(found){
                        System.out.println("Number " + num + " occured " + count + " times in the array");
		}
		else{
                        System.out.println("Number " + num + " not found in array");
                }
        }
}

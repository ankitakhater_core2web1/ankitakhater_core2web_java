import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Size:");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the elements:");

                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }
		System.out.println("Array: ");
                for(int j = 0; j<arr.length ; j++){
			if(arr[j]%2==0){
                             arr[j]=0;
			}
			else{
				arr[j]=1;
			}
			System.out.print(arr[j] + " ");
                }
		System.out.println();
        }
}

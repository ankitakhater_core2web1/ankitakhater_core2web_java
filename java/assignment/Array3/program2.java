import java.util.*;

class Occurance{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
		System.out.println("Enter size:");
                int size = sc.nextInt();
		int arr[] = new int[size];
		
		System.out.println("Enter elements: ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter specific number: ");
		int num = sc.nextInt();
		boolean found = false;

		for(int j = 0; j<arr.length; j++){
			if(num == arr[j]){
                        	System.out.println("Number " + num + " occured at index " + j);
				found = true;
				break;
                	}
		}
		if(!found){
			System.out.println("Number " + num + " not found in array");
		}
        }
}

import java.util.*;

class ArrayNum{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                System.out.println("Corner Elements of an array:");
                for(int i = 0;i<arr.length;i+=2){
		 	for(int j = 0;j<arr.length;j+=2){
                                System.out.print(arr[i][j] + " ");
                	}
		}
                System.out.println();
        }
}

import java.util.*;

class ArrayDiv{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                System.out.println("Array elements which are divisible by 3 are:");
                 for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                if(arr[i][j] % 3 == 0){
                        		System.out.print(arr[i][j] + " ");
				}
			}

                }
		System.out.println();
        }
}

import java.util.*;

class ArraySum{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();
                int sum =0;

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                System.out.println("Sum of Secondary diagonal of an array:");
                 for(int i = 0;i<arr.length;i++){
                        sum+= arr[i][row-i-1];
                 }
                 System.out.println(sum);
        }
}

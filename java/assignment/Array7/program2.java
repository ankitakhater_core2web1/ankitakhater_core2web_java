import java.util.*;

class ArraySum{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();
		int sum = 0;

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                 for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                sum = arr[i][j]+sum;
                        }
                }
		System.out.println("Sum of Arry Elements is: " + sum);
        }
}

import java.util.*;

class ArrayProduct{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();
		int product = 1;

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                System.out.println("Product of  primary diagonal of an array:");
                 for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                if(i==j){
					product = arr[i][j] * product;
                                        
                                }
                        }

                }
                System.out.println(product);
        }
}

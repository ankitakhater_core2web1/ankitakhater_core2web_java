import java.util.*;

class ArrayProduct{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();
                int sum1 =0;
		int sum2=0;

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                System.out.println("Product of sum of primary and secondary diagonal of an array:");
                 for(int i = 0;i<arr.length;i++){
				sum1 += arr[i][i];

                }
		for(int i = 0;i<arr.length;i++){
                                sum2 += arr[i][row-1-i];

                }
		int product = sum1 * sum2;
                System.out.println(product);
        }
}

import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("size of rows: ");
                int row = sc.nextInt();
                System.out.println("size of columns: ");
                int column = sc.nextInt();
		int num = 1;

                int arr[][] = new int[row][column];
                System.out.println("Enter elements in row and column");
                for(int i = 0;i<arr.length;i++){
                        for(int j=0;j<arr[i].length;j++){
                                arr[i][j] = sc.nextInt();
                        }
                }
                 for(int i = 0;i<arr.length;i++){
			 int sum = 0;
                        for(int j=0;j<arr[i].length;j++){
				sum = sum+arr[i][j];
				
                        }
                        System.out.println("sum of row " + num + " = " + sum);
			num++;
                }
        }
}

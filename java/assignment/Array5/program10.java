import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.nextInt();
                }
                System.out.println("The factorial of each number: ");
                for(int j = 0; j<arr.length;j++){
			 int count = 1;
                        for(int k = 1; k<=arr[j]; k++){
                              count = k*count;
			        
                        }
			System.out.print(count + ", ");
                }
		System.out.println();
        }
}

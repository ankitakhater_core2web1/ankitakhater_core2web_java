import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();
		int max = 0;

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.nextInt();
                }
		Arrays.sort(arr);
                System.out.println("The second minimum number in an array is: " + arr[1]);            
        }
}

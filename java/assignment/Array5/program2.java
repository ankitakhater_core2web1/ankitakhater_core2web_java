import java.util.*;

class ArraySum{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Size:");
                int size = sc.nextInt();
                int even = 0;
		int odd = 0;

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }

                for(int j =0; j<arr.length ; j++){
                        if(arr[j] % 2 == 0){
                              even = even + arr[j];
                        }
                        else{
                              odd = odd + arr[j];
                        }
                }
                        System.out.println("Odd Sum: " + odd);
                        System.out.println("Even Sum: " + even);
        }
}

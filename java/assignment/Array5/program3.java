import java.util.*;

class ArrayPalindrome{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size:");
                int size = sc.nextInt();
		int flag = 0;

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<size; i++){
                        arr[i]=sc.nextInt();
                }

                for(int j = 0; j<arr.length ; j++){
			if(arr[j]==arr[size-j-1]){
				flag = 0;
			}
			else {
				flag = 1;
			}
		}
		if(flag==0){
                       	System.out.println("Given array is palindrome");
                }
                else{
                       	System.out.println("Given array is not palindrome");
               	}
	}
}

import java.util.*;

class Array{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Size:");
                int size = sc.nextInt();
		int flag = 0;

                int arr[] = new int[size];

                System.out.println("Enter the elements:");
                for(int i = 0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }

                for(int j = 1; j<arr.length ; j++){
                        if(arr[j-1] >= arr[j]){
			      flag = 0;
			      break;
			}
		        else{
			      flag = 1;
			}
                }
		if(flag == 0){
			System.out.println("Given array is not in ascending order");
		}
		else{
			System.out.println("Given array is in ascending order");
		}
        }
}
